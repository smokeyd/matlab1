function thetaHit = Target ( coords )
% 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                           %
%                      // \                 %
%                      \\_/ //              %
%    ''-.._.-''-.._.. -(||)(')              %
%                      '''                  %
%    APPM 3050 | Spring 2016 | Project 1    %
%  Dave Levin & Karl Forst & Dominik Arndt  %
%                                           %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Global variables for trajectory calculation

global debug;
global attrs;
global xt;
global yt;

attrs = Sensors();
xt = coords(1);
yt = coords(2);

%% Set initial theta values

thetaA = 10;
thetaB = 80;

thetaHit = Bisection(thetaA,thetaB);

end