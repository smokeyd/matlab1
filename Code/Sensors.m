function attrs = Sensors( )

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                           %
%                      // \                 %
%                      \\_/ //              %
%    ''-.._.-''-.._.. -(||)(')              %
%                      '''                  %
%    APPM 3050 | Spring 2016 | Project 1    %
%  Dave Levin & Karl Forst & Dominik Arndt  %
%                                           %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Column 1: (alpha/m) of attractor [strength/intensity]
% Column 2: x-coord of attractor
% Column 3: y-coord of attractor

attrs = [-1e06, 200, 1000;
          1e06, 800, 600 ];

end

