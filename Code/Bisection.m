function c = Bisection( a, b )

% Implementation of Bisection to find the root of Distance(x)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                           %
%                      // \                 %
%                      \\_/ //              %
%    ''-.._.-''-.._.. -(||)(')              %
%                      '''                  %
%    APPM 3050 | Spring 2016 | Project 1    %
%  Dave Levin & Karl Forst & Dominik Arndt  %
%                                           %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

fa = Distance(a);
fb = Distance(b);

if fa*fb > 0
   disp(['Wrong initial thetas: ', num2str(fa), ',', num2str(fb)])
   return;
end
     
% Initial guesses for angle
c = (a+b)/2
fc = Distance(c);

% Vanilla bisection implementation
while abs(fc) > 10^-3 % arbitrary threshold that has been set
    if fa*fc < 0;
        b = c;
    else
        a = c;
    end
    c = (a+b)/2
    deg2rad(c)
    fc = Distance(c);
end