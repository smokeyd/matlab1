function y = FireTest()
% Test our theta calculations against known trials

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                           %
%                      // \                 %
%                      \\_/ //              %
%    ''-.._.-''-.._.. -(||)(')              %
%                      '''                  %
%    APPM 3050 | Spring 2016 | Project 1    %
%  Dave Levin & Karl Forst & Dominik Arndt  %
%                                           %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%  Tests the Target functions against their known value

global attrs;
global debug;



attrs = Sensors();
debug = true;

expected = [ 1.373, 1.373]
target =  [ 100,  500]

   figure;
   hold on;
   
   plot(target(1), target(2), 'x', 'Color', 'red') 
       
   tic
   disp(['Target X,Y: ', num2str(target(1)), ', ', num2str(target(2))])
   disp(['Returned Value: ', num2str(deg2rad(Target(target)))])
   disp(['Expected Values: ', num2str(expected(1)), ', ', num2str(expected(2))])
   toc