function minDist = Distance( theta )
% Returns minimum distance from target to projectile fired at angle theta.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                           %
%                      // \                 %
%                      \\_/ //              %
%    ''-.._.-''-.._.. -(||)(')              %
%                      '''                  %
%    APPM 3050 | Spring 2016 | Project 1    %
%  Dave Levin & Karl Forst & Dominik Arndt  %
%                                           %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Input:
%   theta: Firing angle (degrees)

% Output:
%   minDist: Closest distance from a projectile fired at angle theta, to
%   the target.

%% Global Parameters & Initial conditions

format long

global debug;
global xt;
global yt;
global dist;

beta = 500;%50                 % # of points to sample between origin and x-value of target
tInterval = [0, 1000];%0,20

xInit(1) = 0;      % x = 0
xInit(2) = 0;      % y = 0
xInit(3) = 1500;   % v = 1500
xInit(4) = deg2rad(theta);  % Theta = theta 

%% Integrate the ODE system

ode_options = odeset('Events',  @stopcond,...
                     'MaxStep', (1/50) );
            
[t, x, te, xe, ie] = ode45( @MySystem, tInterval, xInit, ode_options );

%% Graph Results

hold on;
plot(x(:,1), x(:,2))%, 'o')
%hold off;

%% Calculate distance to target from each sampled point of projectile; return minimum
dist = ((xt-x(:,1)).^2 + (yt-x(:,2)).^2 ).^(1/2);

[ minDist, minDist_idx ] = min(dist);

minDist = minDist;

x(minDist_idx,:);
minDist;

if (x(minDist_idx,2)  <  yt) 
    minDist = -minDist;                   % Undershot
end
