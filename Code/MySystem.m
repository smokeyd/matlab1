function xdot = MySystem( t, x )
% System of DiffEq's for xy position, velocity, and trajectory angle

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                           %
%                      // \                 %
%                      \\_/ //              %
%    ''-.._.-''-.._.. -(||)(')              %
%                      '''                  %
%    APPM 3050 | Spring 2016 | Project 1    %
%  Dave Levin & Karl Forst & Dominik Arndt  %
%                                           %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Given constants
global attrs;

g  = 9.81;  % gravity
cm = 0.002; % coefficient of drag

%% Attractor forces

% distance from projectile to attractor
d = sqrt((attrs(:,2)-x(1)).^2 + (attrs(:,3)-x(2)).^2);

% forces on projectile in the tangential and normal direction
Ft = 0*( attrs(:,1)/(d.^3) ) * ( (x(1)-attrs(:,2))*cos(x(4)) + (x(2)-attrs(:,3))*sin(x(4)) );
Fn = 0*(1/x(3)) * ( attrs(:,1)/(d.^3) ) * (-(x(1)-attrs(:,2))*sin(x(4)) + (x(2)-attrs(:,3))*cos(x(4)) );

%% Coupled equations

xdot(1) = x(3)*cos(x(4));                            % x'
xdot(2) = x(3)*sin(x(4));                            % y'
xdot(3) = (-cm)*(x(3)^2) - g*sin(x(4)) + sum(Ft);    % v'
xdot(4) = (-g*cos(x(4))/x(3)) - sum(Fn);                    % theta'

% Return results as column vector
xdot = xdot';