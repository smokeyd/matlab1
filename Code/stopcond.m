function [value, stopInteg, direction] = stopcond(t,x)
% Stop conditions for ode45 called by Distance.m

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                           %
%                      // \                 %
%                      \\_/ //              %
%    ''-.._.-''-.._.. -(||)(')              %
%                      '''                  %
%    APPM 3050 | Spring 2016 | Project 1    %
%  Dave Levin & Karl Forst & Dominik Arndt  %
%                                           %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

global yt;
global xt; 

value = [ (1.5 * xt - x(1));(x(2)) ];

if ( any(value <= 0) )
    value = [0; 0];
end
    
stopInteg = [1; 1];
direction = [0; 0];
